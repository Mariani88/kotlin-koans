package iii_conventions

import java.util.*


data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {

    override fun compareTo(other: MyDate): Int {
        return dayOfMonth - other.dayOfMonth
    }
}

operator fun MyDate.rangeTo(other: MyDate): DateRange = DateRange(this, other)

enum class TimeInterval {
    DAY,
    WEEK,
    YEAR
}

class DateRange(val start: MyDate, val endInclusive: MyDate) : Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> {
        return DateIterator(start, endInclusive)
    }

    class DateIterator(firstDate: MyDate, endInclusive: MyDate) : Iterator<MyDate> {
        var currentDate = firstDate
        val last = endInclusive

        override fun next(): MyDate {
            val date = currentDate
            currentDate = currentDate.nextDay()
            return date
        }

        override fun hasNext(): Boolean {
            return DateRange(currentDate, last).contains(currentDate)
        }
    }

    operator fun contains(date: MyDate): Boolean {
        val startDate = toDate(start)
        val endDate = toDate(endInclusive)
        val date = toDate(date)
        return date.after(Date(startDate.time - 1)) && date.before(Date(endDate.time + 1))
    }

    private fun toDate(value: MyDate): Date {
        val calendar = Calendar.getInstance()
        calendar.set(value.year, value.month, value.dayOfMonth)
        return calendar.time
    }
}